package com.RSPL.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.RSPL.entity.ProductSku;
import com.RSPL.service.ProductSkuService;

/**
 * 
 * @author nisha.tiwari
 *
 */

@RestController
public class ProductSkuController {
	
	@Autowired
	ProductSkuService productSkuService;
	
//	Get request handler to fetch all the details
	@GetMapping("/productSku")
	public ResponseEntity<List<ProductSku>> getAllProductSkuDetails() {
		try{
			List<ProductSku> productSkuList = productSkuService.getAllProductSkuDetails();
			return ResponseEntity.ok(productSkuList);
		} catch(Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
//	Get request handler to fetch a particular project
	@GetMapping("/productSku/{id}")
	public ResponseEntity<Optional<ProductSku>> getProductSkuDetail(@PathVariable Integer id) { 
		try {
			Optional<ProductSku> productSku = productSkuService.getProductSkuDetail(id);
			return ResponseEntity.ok(productSku);
		} catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
	
//	Post request handler to save data sent through request
	@PostMapping("/productSku")
	public ResponseEntity<Void> saveProductSkuDetails(@RequestBody ProductSku productSku) {
		try{
			productSkuService.saveProductSkuDetails(productSku);
			System.out.println(productSku);
			return ResponseEntity.status(HttpStatus.OK).build();
		} catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
	
//	Delete request handler to delete a particular record
	@DeleteMapping("/productSku/{id}")
	public ResponseEntity<Void> deleteProductSkuDetail(@PathVariable Integer id) {
		try {
			productSkuService.deleteProductSkuDetail(id);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}catch(Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
//	Put request handler to update a particular ProductSku record
//	@PutMapping("/productSku/{id}")
//	public ResponseEntity<Void> updateProductSkuDetail(@RequestBody ProductSku productSku, @PathVariable Integer id) {
//		try {
//			productSkuService.updateProductSkuDetail(productSku, id);
//			return ResponseEntity.ok().build();
//		} catch(Exception e) {
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
//		}
//	}
	
}
