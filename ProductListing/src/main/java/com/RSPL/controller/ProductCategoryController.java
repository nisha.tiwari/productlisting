package com.RSPL.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.RSPL.entity.ProductCategory;
import com.RSPL.service.ProductCategoryService;

/**
 * 
 * @author nisha.tiwari
 *
 */

@RestController
public class ProductCategoryController {

	@Autowired
	ProductCategoryService productCategoryService;
	
//	Get request handler to fetch all the categories of products
	@GetMapping("/categories")
	public ResponseEntity<List<ProductCategory>> getAllProductCategories() {
		try {
			List<ProductCategory> productCategories = productCategoryService.getAllProductCategories();
			System.out.println(productCategories);
			return ResponseEntity.ok(productCategories);
		}
		catch(Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
//	Get request handler to fetch a particular category
	@GetMapping("/categories/{id}")
	public ResponseEntity<Optional<ProductCategory>> getProductCategory(@PathVariable Integer id) {
		try {
			Optional<ProductCategory> productCategory = productCategoryService.getProductCategory(id);
			return ResponseEntity.ok(productCategory);
		} catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
	
//	Post request handler to save a product category
	@PostMapping("/categories")
	public ResponseEntity<Void> saveProductCategory(@RequestBody ProductCategory productCategory) {
		try {
			productCategoryService.saveProductCategory(productCategory);
			System.out.println(productCategory);
			return ResponseEntity.status(HttpStatus.OK).build();
		} catch(Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
	
//	Delete request handler to delete a particular category based on id
	@DeleteMapping("/categories/{id}")
	public ResponseEntity<Void> deleteProductCategory(@PathVariable Integer id) {
		try {
			productCategoryService.deleteProductCategory(id);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
	
//	Put request handler to update a particular project
//	@PutMapping("/categories/{id}")
//	public ResponseEntity<Void> updateProductCategory(@RequestBody ProductCategory productCategory, @PathVariable Integer id) {
//		try {
//			productCategoryService.updateProductCategory(productCategory, id);;
//			return ResponseEntity.status(HttpStatus.OK).build();
//		} catch(Exception e) {
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
//		}
//	}
}
