package com.RSPL.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.RSPL.entity.ProductSku;

/**
 * 
 * @author nisha.tiwari
 *
 */

@Repository
public interface ProductSkuRepository extends JpaRepository<ProductSku, Integer> {

}
