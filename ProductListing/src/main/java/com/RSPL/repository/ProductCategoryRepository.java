package com.RSPL.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.RSPL.entity.ProductCategory;

/**
 * 
 * @author nisha.tiwari
 *
 */

@Component
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer>{

	
}
