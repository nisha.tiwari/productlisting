package com.RSPL.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.RSPL.entity.ProductSku;
import com.RSPL.repository.ProductSkuRepository;

/**
 * 
 * @author nisha.tiwari
 *
 */

@Service
public class ProductSkuService {
	
	@Autowired
	ProductSkuRepository productSkuRepository;
	
	public List<ProductSku> getAllProductSkuDetails() {
		return productSkuRepository.findAll();
	}
	
	public Optional<ProductSku> getProductSkuDetail(Integer id) {
		return productSkuRepository.findById(id);
	}
	
	public void saveProductSkuDetails(ProductSku productSku) {
		productSkuRepository.save(productSku);
	}
	
	public void deleteProductSkuDetail(Integer id) {
		productSkuRepository.deleteById(id);
	}
	
//	public void updateProductSkuDetail(ProductSku productSku, Integer id) {
//		productSku.setId(id);
//		productSkuRepository.save(productSku);
//	}

}
