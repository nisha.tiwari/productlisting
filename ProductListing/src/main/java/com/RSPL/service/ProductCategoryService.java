package com.RSPL.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.RSPL.entity.ProductCategory;
import com.RSPL.repository.ProductCategoryRepository;


/**
 * 
 * @author nisha.tiwari
 *
 */

@Component
public class ProductCategoryService {

	@Autowired
	ProductCategoryRepository productCategoryRepository;
	
//	Fetch all the categories of products
	public List<ProductCategory> getAllProductCategories() {
		return productCategoryRepository.findAll();
	}
	
//	Fetch a particular category
	public Optional<ProductCategory> getProductCategory(Integer id) {
		Optional<ProductCategory> productCategory = productCategoryRepository.findById(id);
		return productCategory;
	}
	
//	Save a category
	public void saveProductCategory(ProductCategory productCategory) {
		productCategoryRepository.save(productCategory);
	}
//	Delete a particular category
	public void deleteProductCategory(Integer id) {
		productCategoryRepository.deleteById(id);
	}

//	Update a particular project
//	public void updateProductCategory(ProductCategory productCategory, Integer id) {
//		productCategory.
//		productCategoryRepository.save(productCategory);
//	}
	
	
	
}
