package com.RSPL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.RSPL.repository.ProductCategoryRepository;
import com.RSPL.repository.ProductSkuRepository;

/**
 * 
 * @author nisha.tiwari
 *
 */

@SpringBootApplication
public class ProductListingApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(ProductListingApplication.class, args);
	
		System.out.println("ok");
		
	}

}
